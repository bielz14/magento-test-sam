<?php

class RP_Block_Adminhtml_Requestprice_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $newValue = NULL;
        $value = trim((string)$row->getData($this->getColumn()->getIndex()));
        if (!empty($value) || $value == 0) {
            switch ($value) {
                case 0:
                    $newValue = 'New';
                    break;
                case 1:
                    $newValue = 'In Progress';  
                    break;
                case 2:
                    $newValue = 'Closed';
                    break;
            }

            return $newValue;
        }
    }
}