<?php

class RP_Block_Adminhtml_Requestprice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_manufacturer;
    protected $_store;

    public function __construct()
    {
        parent::__construct();
        $this->setId('rpRequestpriceGrid');
        $this->setDefaultSort('requestprice_identifier');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('rp/requestprice')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('rp')->__('ID'),
            'align'     => 'left',
            'index'     => 'id',
            'type'      => 'number',
        ));

        $this->addColumn('username', array(
            'header'    => Mage::helper('rp')->__('Username'),
            'align'     => 'left',
            'index'     => 'username',
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('rp')->__('Email'),
            'align'     => 'left',
            'index'     => 'email'
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('rp')->__('Status'),
            'align'     => 'left',
            'index'     => 'status',
            'renderer'  => 'RP_Block_Adminhtml_Requestprice_Renderer_Status',
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('rp')->__('Created At'),
            'align'     => 'left',
            'index'     => 'created_at'
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('rp')->__('Action'),
            'align'     => 'left',
            'index'     => 'id',
            'type'      => 'action',
            'actions'   =>  array(
                    array(
                        'caption' => Mage::helper('rp')->__('Edit'),
                        'url' => array('base'=> '*/*/edit'),
                        'field' => 'id'
                    )
            ),
            'filter'    => false,
            'sortable'  => false
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) 
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
         
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('tax')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => Mage::helper('tax')->__('Are you sure?')
        ));
         
        return $this;
    }
}