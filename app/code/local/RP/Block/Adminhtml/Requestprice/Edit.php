<?php

class RP_Block_Adminhtml_Requestprice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_requestprice';
        $this->_blockGroup = 'rp';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('rp')->__('Save Requestprice'));
        $this->_updateButton('delete', 'label', Mage::helper('rp')->__('Delete Requestprice'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('rp')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "


            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('requestprice')->getId()) {
            return Mage::helper('rp')->__("Edit Requestprice");
        }
        else {
            return Mage::helper('rp')->__('New Requestprice');
        }
    }

    protected function _prepareLayout()
    {
        $this->setChild('form',
            $this->getLayout()->createBlock( $this->_blockGroup . '/' . $this->_controller . '_edit_tab_form',
            $this->_controller . '._edit_tab_form')->setSaveParametersInSession(true) );
        return parent::_prepareLayout();
    }
}