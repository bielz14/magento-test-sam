<?php

class RP_Block_Adminhtml_Requestprice_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $requestprice = Mage::registry('requestprice');
        $form = new Varien_Data_Form(array(
                        'id'         => 'edit_form',
                        'action'     => $this->getUrl('*/*/save', array(
                                'id' => $this->getRequest()->getParam('id')
                            )
                        ),
                        'method'     => 'post',
                        'enctype'    => 'multipart/form-data'
                    )
        );

        $fieldset = $form->addFieldset('requestprice_fieldset', array('legend' => Mage::helper('rp')->__('Requestprice Information')));

        $fieldset->addField('username', 'text', array(
            'name'  => 'username',
            'label' => Mage::helper('rp')->__('Requestprice Name'),
            'id'    => 'username',
            'title' => Mage::helper('rp')->__('Requestprice Name'),
            'required' => true,
        ));

        $fieldset->addField('email', 'text', array(
            'name'  => 'email',
            'label' => Mage::helper('rp')->__('Email'),
            'id'    => 'email',
            'title' => Mage::helper('rp')->__('User Email'),
            'required' => true,
        ));

        $fieldset->addField('sku', 'text', array(
            'name'  => 'sku',
            'label' => Mage::helper('rp')->__('SKU'),
            'id'    => 'sku',
            'title' => Mage::helper('rp')->__('SKU')
        ));

        $fieldset->addField('comment', 'text', array(
            'name'  => 'comment',
            'label' => Mage::helper('rp')->__('Comment'),
            'id'    => 'comment',
            'title' => Mage::helper('rp')->__('Comment')
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('rp')->__('Status'),
            'title' => Mage::helper('rp')->__('Status'),
            'name'  => 'status',
            'values' => [
                [
                    'value' => 0,
                    'label' => Mage::helper('rp')->__('New'),
                ],
                [
                    'value' => 1,
                    'label' => Mage::helper('rp')->__('In Progress'),
                ],
                [
                    'value' => 2,
                    'label' => Mage::helper('rp')->__('Closed'),
                ]
            ],
            'required' => true,
        ));

        $form->setValues($requestprice->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}