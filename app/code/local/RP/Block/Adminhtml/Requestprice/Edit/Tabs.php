<?php

class RP_Block_Adminhtml_Requestprice_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('requestprice_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('rp')->__('Requestprice Information'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('main_section', array(
			'label' => Mage::helper('rp')->__('Requestprice Information'),
			'title' => Mage::helper('rp')->__('Requestprice Information')
		));

		return parent::_beforeToHtml();
	}
}