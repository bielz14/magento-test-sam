<?php

class RP_Block_Adminhtml_Requestprice extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_requestprice';
        $this->_blockGroup = 'rp';
        $this->_headerText = Mage::helper('rp')->__('Requestprice');
        //$this->_addButtonLabel = Mage::helper('rp')->__('Add new requestprice');TODO: removing
        parent::__construct();
    }
}