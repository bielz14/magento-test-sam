<?php

class RP_Model_Resource_Requestprice extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('rp/request_price', 'id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {	
        if ( !$object->getId() ) {
            $object->setCreatedAt($this->formatDate(true));
        }
        $object->setModified($this->formatDate(true));
        return $this;
    }
}