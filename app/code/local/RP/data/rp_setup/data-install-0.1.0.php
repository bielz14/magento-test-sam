<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('rp/request_price')}` (
      `id` int(11) NOT NULL auto_increment,
      `username` varchar(15) NOT NULL,
      `email` text NOT NULL,
      `comment` text default NULL,
      `status` int unsigned NOT NULL default 0,
      `sku` varchar(15) NOT NULL,
      `created_at` datetime NOT NULL default '0000-00-00 00:00:00',
      PRIMARY KEY  (`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8;    
");
$installer->endSetup();