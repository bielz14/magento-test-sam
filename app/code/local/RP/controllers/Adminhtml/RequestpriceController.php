<?php

class RP_Adminhtml_RequestpriceController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {   
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('rp/adminhtml_requestprice'));
        $this->renderLayout();
    }

    public function newAction()
    {   
        $this->_forward('edit');
    }

    public function editAction() 
    {   
        $id = $this->getRequest()->getParam('id');
        Mage::register('requestprice', Mage::getModel('rp/requestprice')->load($id));
        $requestpriceObject = (array)Mage::getSingleton('adminhtml/session')->getRequestpriceObject(true);
        if (count($requestpriceObject)) {
            Mage::registry('requestprice')->setData($requestpriceObject);
        }
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('rp/adminhtml_requestprice_edit'))
        ->_addLeft($this->getLayout()->createBlock('rp/adminhtml_requestprice_edit_tabs'));
        $this->renderLayout();
    }

    public function saveAction()
    {   
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('rp/requestprice')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('rp')->__('This Requestprice no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            $data['id'] = $id;

            try {
                $model->setData($data);
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('rp')->__('The requestprice has been saved.'));
                
                if ($this->getRequest()->getParam('back')) {
                    Mage::getSingleton('adminhtml/session')->setRequestpriceObject($data);
                    $this->_redirect('*/*/edit', array('id' => $id));
                    return;
                }
                
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setRequestpriceObject(false);

                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $requestprice = Mage::getModel('rp/requestprice')
            ->setId($this->getRequest()->getParam('id'))
            ->delete();
        if ($requestprice->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('rp')->__('Requestprice was deleted successfully!'));
        }
        $this->_redirect('*/*/');

    }

    public function massDeleteAction() 
    {
        $requestIds = $this->getRequest()->getParam('id');
        if(!is_array($requestIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select reqeust(s)'));
        } else {
            try {
                foreach ($requestIds as $requestId) {
                    $requestData = Mage::getModel('rp/requestprice')->load($requestId);                    
                    $requestData->delete();                    
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($requestIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}