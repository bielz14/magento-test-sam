<?php
class RP_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {	
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveAction()
    {   
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('rp/requestprice')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('rp')->__('This Requestprice no longer exists.'));
                $this->_redirect('*/*/');
            }

            $data['id'] = $id;

            try {
                $model->setData($data);
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('rp')->__('The requestprice has been saved.'));
                
                echo true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setRequestpriceObject(false);
            }
        }
        echo false;
    }
}